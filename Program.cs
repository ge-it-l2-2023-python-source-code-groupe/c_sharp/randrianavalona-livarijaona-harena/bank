﻿

using System.Security.Cryptography.X509Certificates;
using tp_client;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
        
        Client client1 = new Client();
        Console.WriteLine("Compte 1 : ");
        Console.WriteLine("Donner le CIN : ");
        
        client1.CIN = Console.ReadLine() ?? "";

        Console.WriteLine("Donner le nom : ");
        client1.Nom = Console.ReadLine() ?? "";

        Console.WriteLine("Donner le Prenom : ");
        client1.Prenom = Console.ReadLine() ?? "";

        Console.WriteLine("Donner le numero de telephone : ");
        client1.Tel = Console.ReadLine() ?? "";

        Compte compte1 = new Compte(0, client1);
        Console.WriteLine("Details du compte : ");
        Console.WriteLine("********************");
        compte1.ResumeCompte();
        Console.WriteLine("********************");


        Console.WriteLine("Donner le montant a deposer : ");
        double montantDepot = Convert.ToDouble(Console.ReadLine());
        compte1.Crediter(montantDepot);
        Console.WriteLine("Operation bien effectée");
        Console.WriteLine("**********************");
        compte1.ResumeCompte();
        Console.WriteLine("**********************");

        Console.WriteLine("Donner le montant a retirer : ");
        double montantRetrait = Convert.ToDouble(Console.ReadLine());
        compte1.Debiter(montantRetrait);
        
        Console.WriteLine("**********************");
        compte1.ResumeCompte();
        Console.WriteLine("**********************");

        
        
    }

}
