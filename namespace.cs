namespace tp_client
{
    class Client
{
    

    public string CIN {get; set;}
    public string Tel {get;  set;}
    public string Nom {get;  set;}
    public string Prenom {get;  set;}
    public Client(string cin, string nom, string prenom, string tel){
          CIN = cin;
          Nom = nom;
          Prenom = prenom;
          Tel = tel;
    }
    public Client(string cin, string nom, string prenom){
          CIN = cin;
          Nom = nom;
          Prenom = prenom;
          Tel ="";
    }
    public Client() {
        CIN = "";
          Nom = "";
          Prenom = "";
          Tel ="";
    }
     
    public string Afficher(){
        return $@"
        CIN : {CIN}
        Nom: {Nom}
        Prenom : {Prenom}
        telephone : {Tel}";
    }
}
class Compte
{
    private static int compteur = 0;
    private int code;
    private double solde;
    private Client proprietaire;

    
    public int Code {
        get { return code;}
    }
    public double Solde {
        get{return solde;}
        set{solde = value;}
    }
    public Client Proprietaire {
        get{return proprietaire;}
    }
    public Compte(double solde, Client proprietaire){
        code = ++compteur;
        Solde = solde;
        this.proprietaire = proprietaire;
    } 
    

    public void Crediter(double montant){
        solde += montant;
    }
    public void Debiter(double montant){
        if (solde>montant) 
        {solde -= montant;
        Console.WriteLine("Operation bien effectuée");}
        else
        Console.WriteLine("Votre solde est insuffisant");
    }
    public void Crediter(double montant, Compte compte){
        Crediter(montant);
        compte.Debiter(montant);
    }
    public void Debiter(double montant, Compte compte){
        Debiter(montant);
        compte.Crediter(montant);
    }
    public void ResumeCompte(){
        
        Console.WriteLine("Solde de compte : "+ solde);
        Console.WriteLine("Proprietaire du compte :\nCIN : "+ proprietaire.CIN +"\nNOM : "+ proprietaire.Nom + "\nPrenom : "+ proprietaire.Prenom + "\nTel : " + proprietaire.Tel);
        }
    public static void AfficherNombreComptes(){
        Console.WriteLine("Nombre de compte crees : "+ compteur);
    }
}
}  